use crate::Decimal;

// To be able to faithfully represent a dollars-and-cents value in an `f64`, `abs(value)` must be
// less than the figure below, or we will be subject to precision errors such as n + m = n
// (instead of n + m = n + m).
// --
// # Technical explanation:
// `f64` has a 53-bit mantissa (including the IEEE-754-2008 virtual bit), meaning it can represent
// an integer `abs(value)` of 2^53.  Given that a dollar is comprised of 100 cents, we need 100x
// that precision.  In binary, 100 takes 6.62 bits, or 7 bits to encode.  Subtracting 7 bits from
// the 53-bit mantissa leaves a 46-bit mantissa, which is the largest value that `f64` can hold
// without showing precision errors in the least significant (penny) position.
pub const MAX_ABS_CONTIGUOUS_F64_USD: f64 = 70_368_744_177_664_f64;

pub const MAX_ABS_D96_DOLLARS_CONVERTIBLE_TO_MINOR_UNITS: Decimal = Decimal::from_parts(
    u32::max_value(),
    u32::max_value(),
    u32::max_value(),
    false,
    2,
);

pub const MSG_VALUE_TOO_LARGE: &str = "Value is too large.  It must be convertible to minor units \
(cents) without overflowing 96-bits";
pub const MSG_VALUE_CONTAINS_FRACTIONAL_MINOR_UNITS: &str =
    "Value contains fractional minor units \
(cents).  Rejected to avoid prescribing a rounding policy (Superman III/Office Space Error)";
pub const MSG_NON_POSITIVE_XAND: &str = "Error attempting to create XAND claim representing \
non-positive dollar amount";
pub const MSG_DECIMAL_FROM_PRIMITIVE_TYPE_CAUSED_BY: &str = "Error constructing `Decimal` value \
from primitive type.  Caused by";
pub const MSG_DECIMAL_FROM_PRIMITIVE_TYPE: &str = "Error constructing `Decimal` value from \
primitive type";
pub const MSG_VALUE_TOO_LARGE_FLOAT_PRECISION: &str = "Error: Value too large to convert to float \
without precision errors.";
pub const MSG_FLOAT_FROM_DECIMAL_CAUSED_BY: &str = "Error constructing {float} value from \
primitive type.  Caused by";
pub const MSG_MINOR_NOT_U64: &str = "Value cannot be represented as a u64 of minor units";
pub const MSG_MINOR_UNIT_REPRESENTATION_FAILURE: &str =
    "Value cannot be represented as the target type in minor units";
