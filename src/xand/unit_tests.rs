#![allow(non_snake_case)]
#![allow(clippy::unwrap_used)]
use super::*;

mod constructors;

#[test]
fn add_succeeds() {
    //given
    let xand1 = Xand::from_usd(Usd::from_str("1.49").unwrap()).unwrap();
    let xand2 = Xand::from_usd(Usd::from_str("2.59").unwrap()).unwrap();
    let expected_result = Xand::from_usd(Usd::from_str("4.08").unwrap()).unwrap();

    //when
    let res = xand1 + xand2;

    //then
    assert_eq!(res, expected_result);
}

#[test]
fn add_assign_succeeds() {
    //given
    let mut xand1 = Xand::from_usd(Usd::from_str("1.39").unwrap()).unwrap();
    let xand2 = Xand::from_usd(Usd::from_str("2.49").unwrap()).unwrap();
    let expected_result = Xand::from_usd(Usd::from_str("3.88").unwrap()).unwrap();

    //when
    xand1 += xand2;

    //then
    assert_eq!(xand1, expected_result);
}

#[test]
fn sub_succeeds() {
    //given
    let xand1 = Xand::from_usd(Usd::from_str("9.49").unwrap()).unwrap();
    let xand2 = Xand::from_usd(Usd::from_str("3.59").unwrap()).unwrap();
    let expected_result = Xand::from_usd(Usd::from_str("5.90").unwrap()).unwrap();

    //when
    let res = xand1 - xand2;

    //then
    assert_eq!(res, expected_result);
}

#[test]
fn sub_assign_succeeds() {
    //given
    let mut xand1 = Xand::from_usd(Usd::from_str("9.49").unwrap()).unwrap();
    let xand2 = Xand::from_usd(Usd::from_str("3.59").unwrap()).unwrap();
    let expected_result = Xand::from_usd(Usd::from_str("5.90").unwrap()).unwrap();

    //when
    xand1 -= xand2;

    //then
    assert_eq!(xand1, expected_result);
}

#[test]
#[should_panic(expected = "Subtraction result is negative")]
#[allow(clippy::no_effect_underscore_binding)]
fn sub_result_cannot_be_negative() {
    //given
    let xand1 = Xand::from_usd(Usd::from_str("3.23").unwrap()).unwrap();
    let xand2 = Xand::from_usd(Usd::from_str("10.22").unwrap()).unwrap();

    //then
    let _res = xand1 - xand2;
}

#[test]
#[should_panic(expected = "Subtraction assignment result is negative")]
fn sub_assign_result_cannot_be_negative() {
    //given
    let mut xand1 = Xand::from_usd(Usd::from_str("3.23").unwrap()).unwrap();
    let xand2 = Xand::from_usd(Usd::from_str("10.22").unwrap()).unwrap();

    //then
    xand1 -= xand2;
}
