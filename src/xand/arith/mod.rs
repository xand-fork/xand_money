use crate::Xand;
use std::ops::{Add, AddAssign, Sub, SubAssign};

/// Define arithmetic operations for `Xand`
impl Add for Xand {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        Self::from_decimal(self.amount() + rhs.amount()).unwrap_or_else(|e| unreachable!("{}", e))
    }
}
impl AddAssign for Xand {
    fn add_assign(&mut self, rhs: Self) {
        *self = Self::from_decimal(self.amount() + rhs.amount())
            .unwrap_or_else(|e| unreachable!("{}", e));
    }
}

impl Sub for Xand {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output {
        Self::from_decimal(self.amount() - rhs.amount())
            .unwrap_or_else(|e| panic!("Subtraction result is negative {:?}", e))
    }
}

impl SubAssign for Xand {
    fn sub_assign(&mut self, rhs: Self) {
        *self = Self::from_decimal(self.amount() - rhs.amount())
            .unwrap_or_else(|e| panic!("Subtraction assignment result is negative: {:?}", e));
    }
}
