#![allow(non_snake_case)]
#![allow(clippy::unwrap_used)]
use super::*;

mod constructors;

#[test]
fn as_minor_units__correctly_converts_major_to_minor_units() {
    //given
    let expected_minor_units = Decimal::new(23439, 0);
    let usd = Usd::from_str("234.39").unwrap();

    //when
    let res = usd.into_minor_units();

    //then
    assert_eq!(res, expected_minor_units);
}

#[allow(clippy::float_cmp)]
#[test]
fn try_from__usd_to_f64_valid_input_succeeds() {
    // given
    let input: f64 = 15_149.99_f64;
    let usd = Usd::from_f64_major_units(input).unwrap();
    let sut = f64::try_from;

    // when
    let res = sut(usd);

    // then
    assert_eq!(res.unwrap(), input);
}

#[test]
fn try_from__usd_to_f64_invalid_positive_max_input_fails() {
    // given
    let input = MAX_ABS_CONTIGUOUS_F64_USD + 2.0;
    let usd = Usd::from_f64_major_units(input).unwrap();
    let sut = f64::try_from;

    // when
    let res = sut(usd);

    // then
    assert!(res.is_err());
}

#[test]
fn try_from__usd_to_f64_invalid_negative_max_input_fails() {
    // given
    let input = -MAX_ABS_CONTIGUOUS_F64_USD - 2.0;
    let usd = Usd::from_f64_major_units(input).unwrap();
    let sut = f64::try_from;

    // when
    let res = sut(usd);

    // then
    assert!(res.is_err());
}

#[test]
fn as_major_units_usd__returns_usd_amount_correctly() {
    // given
    let usd = Usd::from_str("1099.99").unwrap();

    // when
    let res = usd.into_major_units();

    // then
    assert_eq!(res, Decimal::new(109_999, 2));
}

#[test]
fn as_major_units_usd__adding_two_succeeds() {
    // given
    let usd1 = Usd::from_i64_minor_units(199).unwrap();
    let usd2 = Usd::from_i64_minor_units(19_998).unwrap();
    let expected_result = Usd::from_i64_minor_units(199 + 19_998)
        .unwrap()
        .into_major_units();

    // when
    let res = usd1.into_major_units() + usd2.into_major_units();

    // then
    assert_eq!(res, expected_result);
}

#[test]
fn as_minor_units_usd__returns_usd_amount_correctly() {
    // given
    let usd = Usd::from_str("1099.99").unwrap();

    // when
    let res = usd.into_minor_units();

    // then
    assert_eq!(res, Decimal::new(109_999, 0));
}

#[test]
fn as_minor_units_usd__adding_two_succeeds() {
    // given
    let usd1 = Usd::from_i64_minor_units(199).unwrap();
    let usd2 = Usd::from_i64_minor_units(19_998).unwrap();
    let expected_result = Usd::from_i64_minor_units(199 + 19_998)
        .unwrap()
        .into_minor_units();

    // when
    let res = usd1.into_minor_units() + usd2.into_minor_units();

    // then
    assert_eq!(res, expected_result);
}

#[test]
fn partial_equality_usd__comparing_two_succeeds() {
    // given
    let usd1 = Usd::from_f64_major_units(1.23).unwrap();
    let usd2 = Usd::from_i64_minor_units(123).unwrap();

    // then
    assert_eq!(usd1, usd2);
}

#[test]
fn conversion_to_u64_works() {
    let usd = Usd::from_f64_major_units(100.01).unwrap();
    let cents = usd.into_u64_minor_units().unwrap();
    assert_eq!(cents, 10001);
}

#[test]
fn negative_conversion_to_u64_fails() {
    let usd = Usd::from_f64_major_units(-100.01).unwrap();
    let cents = usd.into_u64_minor_units();
    assert!(cents.is_err());
    assert!(matches!(cents.unwrap_err(), MoneyError::ValueIsNegative(_)));
}

#[test]
fn conversion_to_i64_works() {
    let usd = Usd::from_i64_minor_units(100).unwrap();
    let cents = usd.into_i64_minor_units().unwrap();
    assert_eq!(cents, 100);
}

#[test]
fn negative_conversion_to_i64_works() {
    let usd = Usd::from_i64_minor_units(-100).unwrap();
    let cents = usd.into_i64_minor_units().unwrap();
    assert_eq!(cents, -100);
}

#[test]
fn conversion_to_i64_from_u64_works() {
    let usd = Usd::from_u64_minor_units(i64::MAX as u64).unwrap();
    let cents = usd.into_i64_minor_units().unwrap();
    assert_eq!(cents, i64::MAX);
}

#[test]
fn overflow_minor_units_i64_representation_fails() {
    let usd = Usd::from_u64_minor_units(u64::MAX).unwrap();
    let representation_error = usd.into_i64_minor_units().unwrap_err();

    let (value, target_type) = match representation_error.clone() {
        MoneyError::MinorUnitRepresentationFailure {
            value,
            target_type_name,
        } => (value, target_type_name),
        _ => panic!("Unexpected error: {:?}", representation_error),
    };

    assert!(value.eq(&Decimal::from_u64(u64::MAX).unwrap()));
    assert_eq!(target_type, "i64".to_string());
}
