use crate::consts::{
    MSG_DECIMAL_FROM_PRIMITIVE_TYPE, MSG_DECIMAL_FROM_PRIMITIVE_TYPE_CAUSED_BY,
    MSG_FLOAT_FROM_DECIMAL_CAUSED_BY, MSG_MINOR_NOT_U64, MSG_MINOR_UNIT_REPRESENTATION_FAILURE,
    MSG_NON_POSITIVE_XAND, MSG_VALUE_CONTAINS_FRACTIONAL_MINOR_UNITS, MSG_VALUE_TOO_LARGE,
    MSG_VALUE_TOO_LARGE_FLOAT_PRECISION,
};
use rust_decimal::{Decimal, Error as RustDecimalError};
use thiserror::Error;

/// Internal xand banks error handling logic for numeric currency values
#[allow(clippy::module_name_repetitions)]
#[derive(Clone, Debug, Error)]
pub enum MoneyError {
    #[error("{}: {:?}", MSG_VALUE_TOO_LARGE, 0)]
    ValueTooLargeToConvertToMinorUnits(Decimal),
    #[error("{}: {:?}", MSG_VALUE_CONTAINS_FRACTIONAL_MINOR_UNITS, 0)]
    ValueContainsFractionalMinorUnits(Decimal),
    #[error("{}: {:?}", MSG_NON_POSITIVE_XAND, 0)]
    CannotCreateNonPositiveXandAmounts(Decimal),
    #[error("{}: {:?}", MSG_DECIMAL_FROM_PRIMITIVE_TYPE_CAUSED_BY, 0)]
    ErrorConstructingDecimalCausedBy(#[from] RustDecimalError),
    #[error("{}.", MSG_DECIMAL_FROM_PRIMITIVE_TYPE)]
    ErrorConstructingDecimal,
    #[error("{}.", MSG_VALUE_TOO_LARGE_FLOAT_PRECISION)]
    ValueTooLargeToConvertToF64WithoutPrecisionErrors(Decimal),
    #[error("{}: {:?}", MSG_FLOAT_FROM_DECIMAL_CAUSED_BY, 0)]
    ErrorConstructingF64CausedBy(Decimal),
    #[error("{}: {:?}", MSG_MINOR_NOT_U64, 0)]
    ValueIsNegative(Decimal),
    #[error(
        "{}: Value: {:?}, Target Type: {}",
        MSG_MINOR_UNIT_REPRESENTATION_FAILURE,
        value,
        target_type_name
    )]
    MinorUnitRepresentationFailure {
        value: Decimal,
        target_type_name: String,
    },
}
